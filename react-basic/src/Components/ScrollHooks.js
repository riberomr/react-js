import React, { useState, useEffect } from 'react'

export default function ScrollHooks (){

    const [scrollY, setScrollY] = useState(0)

    useEffect(()=>{
        //se vuelve a ejecutar con cada render
     //   console.log('Moviendo el scroll')
        
        const detectarScroll = () => setScrollY(window.pageYOffset)

        window.addEventListener('scroll', detectarScroll)

        return () => {
        //    console.log('Component did unmount')
            window.removeEventListener('scroll', detectarScroll)
        }
    })

    useEffect(()=>{
        //se ejecuta una sola vez debido al segundo parametro que es una lista de dependencuas vacias []
        //console.log('Component did mount')
    },[])

    useEffect(()=>{
        //se ejecuta cada vez que scrollY cambia su valor
       // console.log('Component did update')
    },[scrollY])

    return(
        <>
            <h2>Hook useEfect y ciclo de vida</h2>
            <p>Scroll Y del navegador {scrollY}px</p>
        </>
    )
}