import React, { useState, useEffect } from 'react'

function Pokemon({name, avatar}){
    return(
        <figure>
            <img src={avatar} alt={name}></img>
            <figcaption>{name}</figcaption>
        </figure>
    )
}

export default function AjaxHooks(){

    const [pokemons, setPokemons] = useState([])

    // useEffect(()=>{
    //     let url = "https://pokeapi.co/api/v2/pokemon/"
    //     fetch(url)
    //         .then(res => res.json())
    //         .then(json => {
    //             json.results.forEach((el) => {
    //                 fetch(el.url)
    //                     .then(res => res.json())
    //                     .then(json => {
    //                         let pokemon = {
    //                             id: json.id,
    //                             name: json.name,
    //                             avatar: json.sprites.front_default
    //                         }
    //                         setPokemons((pokemons)=> [...pokemons, pokemon])
    //                     }) 
    //             })
    //         })
    // },[])

    useEffect(()=>{
        //async request in useEffect
        let url = "https://pokeapi.co/api/v2/pokemon/"

        const getPokemons = async (url) => {

            let res = await fetch(url)
            let json = await res.json()

            json.results.forEach(async (el) => {
                let res = await fetch(el.url)
                let json = await res.json()

                let pokemon = {
                    id: json.id,
                    name: json.name,
                    avatar: json.sprites.front_default
                }
                setPokemons((pokemons)=> [...pokemons, pokemon])
            })

        }

        getPokemons(url)
    },[])

    return(
        <>
            <h2>Periciones Asíncronas en componentes Funcionales</h2>
            {pokemons.length === 0? (
                <h3>Cargando...</h3>
            ): (
                pokemons.map((item)=>(
                    <Pokemon
                        key={Math.random()} 
                        name={item.name}
                        avatar={item.avatar}
                    />
                ))
            )}
        </>
    )
}