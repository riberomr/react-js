import logo from './logo.svg';
import './App.css';
import ContadorHooks from './components/ContadorHooks';
import ScrollHooks from './components/ScrollHooks';
import RelojHooks from './components/RelojHooks';
import AjaxHooks from './components/AjaxHooks';
import HooksPersonalizados from './components/HooksPersonalizados';
import Referencias from './components/Referencias';
import Formularios from './components/Formularios';
import Estilos from './components/Estilos';
import ComponentesEstilizados from './components/ComponentesEstilizados';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <section>
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </section>
        <section style={{width: "100%"}}>
          <hr/>
          <ContadorHooks titulo='seguidores'/>
          <hr/>
          <ScrollHooks/>
          <hr/>
          <RelojHooks/>
          <hr/>
          <AjaxHooks />
          <hr/>
          <HooksPersonalizados/>
          <hr/>
          <Referencias/>
          <hr/>
          <Formularios/>
          <br/>
          <br/>
          <hr/>
          <Estilos/>
          <br/>
          <br/>
          <hr/>
          <ComponentesEstilizados/>
        </section>
      </header>
    </div>
  );
}

export default App;
